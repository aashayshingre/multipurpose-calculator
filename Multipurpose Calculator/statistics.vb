﻿Public Class statistics
    Dim boxListRaw As ArrayList = New ArrayList()
    Dim rawRow As Integer = 10
    Dim groupListX As ArrayList = New ArrayList()
    Dim groupListY As ArrayList = New ArrayList()
    Dim groupRow As Integer = 10
    Dim startList As ArrayList = New ArrayList()
    Dim endList As ArrayList = New ArrayList()
    Dim freqList As ArrayList = New ArrayList()
    Dim classRow As Integer = 10
    Private Sub statistics_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Integer = 0
        'appending 20 elements in Tabcontrol1 - raw data
        For i = 1 To 9
            Dim x As New TextBox()
            Dim y As New TextBox()
            Dim z As New TextBox()
            Dim offset As Point = Me.TabControl1.Location
            x.Location = New Point(offset.X + 40, offset.Y + 30 * i)
            y.Location = New Point(offset.X + 180, offset.Y + 30 * i)
            z.Location = New Point(offset.X + 320, offset.Y + 30 * i)
            boxListRaw.Add(x)
            boxListRaw.Add(y)
            boxListRaw.Add(z)
            TabPage1.Controls.Add(x)
            TabPage1.Controls.Add(y)
            TabPage1.Controls.Add(z)
        Next
        'adding elements in Grouped Data (TabControl2)
        For i = 1 To 9
            Dim x1 As New TextBox()
            Dim y1 As New TextBox()
            Dim x2 As New TextBox()
            Dim y2 As New TextBox()
            Dim offset1 As Point = Me.TabControl1.Location
            x1.Location = New Point(offset1.X + 10, offset1.Y + 30 * i)
            y1.Location = New Point(offset1.X + 115, offset1.Y + 30 * i)
            x2.Location = New Point(offset1.X + 240, offset1.Y + 30 * i)
            y2.Location = New Point(offset1.X + 345, offset1.Y + 30 * i)
            groupListX.Add(x1)
            groupListY.Add(y1)
            groupListX.Add(x2)
            groupListY.Add(y2)
            TabPage2.Controls.Add(x1)
            TabPage2.Controls.Add(y1)
            TabPage2.Controls.Add(x2)
            TabPage2.Controls.Add(y2)
        Next
        'adding inputboxes in Frequency Distribution - TabControl3
        For i = 1 To 9
            Dim startInt1 As New TextBox()
            Dim endInt1 As New TextBox()
            Dim freq1 As New TextBox()
            Dim startInt2 As New TextBox()
            Dim endInt2 As New TextBox()
            Dim freq2 As New TextBox()
            startInt1.Width = 50
            startInt2.Width = 50
            endInt1.Width = 50
            endInt2.Width = 50
            freq1.Width = 70
            freq2.Width = 70
            Dim offset2 As Point = Me.TabControl1.Location
            startInt1.Location = New Point(offset2.X + 25, offset2.Y + 30 * i)
            endInt1.Location = New Point(offset2.X + 80, offset2.Y + 30 * i)
            freq1.Location = New Point(offset2.X + 140, offset2.Y + 30 * i)
            startInt2.Location = New Point(offset2.X + 265, offset2.Y + 30 * i)
            endInt2.Location = New Point(offset2.X + 320, offset2.Y + 30 * i)
            freq2.Location = New Point(offset2.X + 380, offset2.Y + 30 * i)
            startList.Add(startInt1)
            endList.Add(endInt1)
            freqList.Add(freq1)
            startList.Add(startInt2)
            endList.Add(endInt2)
            freqList.Add(freq2)
            TabPage3.Controls.Add(startInt1)
            TabPage3.Controls.Add(endInt1)
            TabPage3.Controls.Add(freq1)
            TabPage3.Controls.Add(startInt2)
            TabPage3.Controls.Add(endInt2)
            TabPage3.Controls.Add(freq2)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TabControl1.SelectedTab Is TabPage1 Then
            Dim x As New TextBox()
            Dim y As New TextBox()
            Dim z As New TextBox()
            Dim offset As Point = Me.TabControl1.Location
            x.Location = New Point(offset.X + 40, offset.Y + 30 * rawRow)
            y.Location = New Point(offset.X + 180, offset.Y + 30 * rawRow)
            z.Location = New Point(offset.X + 320, offset.Y + 30 * rawRow)
            boxListRaw.Add(x)
            boxListRaw.Add(y)
            boxListRaw.Add(z)
            TabPage1.Controls.Add(x)
            TabPage1.Controls.Add(y)
            TabPage1.Controls.Add(z)
            rawRow += 1
        ElseIf TabControl1.SelectedTab Is TabPage2 Then
            Dim x1 As New TextBox()
            Dim y1 As New TextBox()
            Dim x2 As New TextBox()
            Dim y2 As New TextBox()
            Dim offset1 As Point = Me.TabControl1.Location
            x1.Location = New Point(offset1.X + 10, offset1.Y + 30 * groupRow)
            y1.Location = New Point(offset1.X + 115, offset1.Y + 30 * groupRow)
            x2.Location = New Point(offset1.X + 240, offset1.Y + 30 * groupRow)
            y2.Location = New Point(offset1.X + 345, offset1.Y + 30 * groupRow)
            groupListX.Add(x1)
            groupListY.Add(y1)
            groupListX.Add(x2)
            groupListY.Add(y2)
            TabPage2.Controls.Add(x1)
            TabPage2.Controls.Add(y1)
            TabPage2.Controls.Add(x2)
            TabPage2.Controls.Add(y2)
            groupRow += 1
        Else
            Dim startInt1 As New TextBox()
            Dim endInt1 As New TextBox()
            Dim freq1 As New TextBox()
            Dim startInt2 As New TextBox()
            Dim endInt2 As New TextBox()
            Dim freq2 As New TextBox()
            startInt1.Width = 50
            startInt2.Width = 50
            endInt1.Width = 50
            endInt2.Width = 50
            freq1.Width = 70
            freq2.Width = 70
            Dim offset2 As Point = Me.TabControl1.Location
            startInt1.Location = New Point(offset2.X + 25, offset2.Y + 30 * classRow)
            endInt1.Location = New Point(offset2.X + 80, offset2.Y + 30 * classRow)
            freq1.Location = New Point(offset2.X + 140, offset2.Y + 30 * classRow)
            startInt2.Location = New Point(offset2.X + 265, offset2.Y + 30 * classRow)
            endInt2.Location = New Point(offset2.X + 320, offset2.Y + 30 * classRow)
            freq2.Location = New Point(offset2.X + 380, offset2.Y + 30 * classRow)
            startList.Add(startInt1)
            endList.Add(endInt1)
            freqList.Add(freq1)
            startList.Add(startInt2)
            endList.Add(endInt2)
            freqList.Add(freq2)
            TabPage3.Controls.Add(startInt1)
            TabPage3.Controls.Add(endInt1)
            TabPage3.Controls.Add(freq1)
            TabPage3.Controls.Add(startInt2)
            TabPage3.Controls.Add(endInt2)
            TabPage3.Controls.Add(freq2)
            classRow += 1
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If TabControl1.SelectedTab Is TabPage1 Then
            For Each ip In boxListRaw
                ip.Clear()
            Next
        ElseIf TabControl1.SelectedTab Is TabPage2 Then
            For Each ib In groupListX
                ib.Clear()
            Next
            For Each ib In groupListY
                ib.Clear()
            Next
        Else
            For Each ib In startList
                ib.Clear()
            Next
            For Each ib In endList
                ib.Clear()
            Next
            For Each ib In freqList
                ib.Clear()
            Next
        End If
        maximum.Clear()
        minimum.Clear()
        midrange.Clear()
        sum.Clear()
        count.Clear()
        mean.Clear()
        median.Clear()
        mode.Clear()
        variance.Clear()
        st_deviation.Clear()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If TabControl1.SelectedTab Is TabPage1 Then
            Dim list1 As ArrayList = New ArrayList()
            For Each ip In boxListRaw
                If ip.Text <> "" Then
                    list1.Add(CDec(ip.Text))
                End If
            Next
            list1.Sort()
            maximum.Text = list1(list1.Count - 1)
            minimum.Text = list1(0)
            midrange.Text = (list1(list1.Count - 1) + list1(0)) / 2
            Dim i As Integer = 0
            Dim total As Decimal = 0
            For i = 0 To list1.Count - 1
                total += list1(i)
            Next
            sum.Text = total
            count.Text = list1.Count
            mean.Text = Math.Round(total / (list1.Count), 8)
            If ((list1.Count) Mod 2 <> 0) Then
                median.Text = list1((list1.Count) / 2)
            Else
                Dim f As Decimal = list1(list1.Count / 2)
                Dim l As Decimal = list1((list1.Count / 2) - 1)
                median.Text = (f + l) / 2
            End If
            mode.Text = "--"
            ' variance
            Dim totalDifference As Decimal = 0
            For i = 0 To list1.Count - 1
                totalDifference += Math.Pow((list1(i) - (total / (list1.Count))), 2)
            Next
            variance.Text = Math.Round(totalDifference / (list1.Count - 1), 8)
            ' standard deviation
            st_deviation.Text = Math.Round(Math.Sqrt(totalDifference / (list1.Count - 1)), 8)
        ElseIf TabControl1.SelectedTab Is TabPage2 Then
            Dim list2 As ArrayList = New ArrayList()
            Dim list3 As ArrayList = New ArrayList()
            For Each it In groupListX
                If it.Text <> "" Then
                    list2.Add(CDec(it.Text))
                End If
            Next
            For Each it1 In groupListY
                If it1.Text <> "" Then
                    list3.Add(CDec(it1.Text))
                End If
            Next
            maximum.Text = list2(list2.Count - 1)
            minimum.Text = list2(0)
            midrange.Text = (list2(list2.Count - 1) + list2(0)) / 2
            Dim x As Integer = 0
            Dim sumgroup As Decimal = 0
            For x = 0 To list2.Count - 1
                sumgroup += list2(x) * list3(x)
            Next
            sum.Text = sumgroup
            Dim i1 As Integer = 0
            Dim total1 As Decimal = 0
            For i1 = 0 To list3.Count - 1
                total1 += list3(i1)
            Next
            count.Text = total1
            mean.Text = Math.Round((sumgroup / total1), 8)
            Dim n As Integer = (total1 + 1) / 2
            Dim i2 As Integer = 0
            Dim cf As Integer = 0
            Dim cfreq As ArrayList = New ArrayList()
            For Each it2 In groupListY
                If it2.Text <> "" Then
                    cf += it2.Text
                    cfreq.Add(CDec(cf))
                End If
            Next
            Dim y As Integer = 0
            For y = 0 To cfreq.Count - 1
                If n <= cfreq(y) Then
                    median.Text = list2(y)
                    Exit For
                End If
            Next
            Dim z As Integer = 0
            Dim large As Integer = list3(0)
            For z = 1 To list3.Count - 1
                If large < list3(z) Then
                    large = list3(z)
                    mode.Text = list2(z)
                End If
            Next
            Dim x1 As Integer = 0
            Dim variancesum As Decimal = 0
            For x1 = 0 To list2.Count - 1
                variancesum += (Math.Pow(list2(x1), 2)) * list3(x1)
            Next
            variance.Text = Math.Round(((variancesum - ((sumgroup * sumgroup) / total1)) / total1), 8)
            st_deviation.Text = Math.Round(Math.Sqrt(((variancesum - ((sumgroup * sumgroup) / total1)) / total1)), 8)
        Else
            Dim startinterval As ArrayList = New ArrayList()
                Dim endinterval As ArrayList = New ArrayList()
                Dim frequency As ArrayList = New ArrayList()
                For Each it In startList
                    If it.Text <> "" Then
                        startinterval.Add(CDec(it.Text))
                    End If
                Next
                For Each it1 In endList
                    If it1.Text <> "" Then
                        endinterval.Add(CDec(it1.Text))
                    End If
                Next
                For Each it2 In freqList
                    If it2.Text <> "" Then
                        frequency.Add(CDec(it2.Text))
                    End If
                Next
                maximum.Text = endinterval(endinterval.Count - 1)
                minimum.Text = startinterval(0)
                midrange.Text = (endinterval(endinterval.Count - 1) + startinterval(0)) / 2
                Dim mid As ArrayList = New ArrayList()
                Dim i As Integer = 0
                For i = 0 To startinterval.Count - 1
                    mid.Add(CDec((startinterval(i) + endinterval(i)) / 2))
                Next
                Dim x As Integer = 0
                Dim sumgroup As Decimal = 0
                For x = 0 To mid.Count - 1
                    sumgroup += mid(x) * frequency(x)
                Next
                sum.Text = sumgroup
                Dim i1 As Integer = 0
                Dim total1 As Decimal = 0
                For i1 = 0 To frequency.Count - 1
                    total1 += frequency(i1)
                Next
                count.Text = total1
                mean.Text = Math.Round((sumgroup / total1), 8)
                Dim n As Decimal = total1 / 2
                Dim i2 As Integer = 0
                Dim cf As Integer = 0
                Dim cfreq As ArrayList = New ArrayList()
                For Each it2 In freqList
                    If it2.Text <> "" Then
                        cf += it2.Text
                        cfreq.Add(CDec(cf))
                    End If
                Next
                Dim y As Integer = 0
                For y = 0 To cfreq.Count - 1
                    If n <= cfreq(y) Then
                        median.Text = Math.Round((startinterval(y) + ((n - (cfreq(y - 1))) * (endinterval(0) - startinterval(0))) / frequency(y)), 8)
                        Exit For
                    End If
                Next
                Dim z As Integer = 0
                Dim large As Integer = frequency(0)
                For z = 1 To frequency.Count - 1
                    If large < frequency(z) Then
                        large = frequency(z)
                        mode.Text = mid(z)
                    End If
                Next
                Dim x1 As Integer = 0
                Dim variancesum As Decimal = 0
                For x1 = 0 To mid.Count - 1
                    variancesum += (Math.Pow(mid(x1), 2)) * frequency(x1)
                Next
                variance.Text = Math.Round(((variancesum - ((sumgroup * sumgroup) / total1)) / total1), 8)
                st_deviation.Text = Math.Round(Math.Sqrt(((variancesum - ((sumgroup * sumgroup) / total1)) / total1)), 8)
            End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            Console.WriteLine(OpenFileDialog1.FileName)
            Dim sr As New IO.StreamReader(OpenFileDialog1.FileName)
            If RadioButton1.Checked Then
                Dim list1 As ArrayList = New ArrayList()
                While sr.Peek() <> -1
                    list1.Add(CDec(sr.ReadLine()))
                End While
                list1.Sort()
                maximum.Text = list1(list1.Count - 1)
                minimum.Text = list1(0)
                midrange.Text = (list1(list1.Count - 1) + list1(0)) / 2
                Dim i As Integer = 0
                Dim total As Decimal = 0
                For i = 0 To list1.Count - 1
                    total += list1(i)
                Next
                sum.Text = total
                count.Text = list1.Count
                mean.Text = Math.Round(total / (list1.Count), 8)
                If ((list1.Count) Mod 2 <> 0) Then
                    median.Text = list1((list1.Count) / 2)
                Else
                    Dim f As Decimal = list1(list1.Count / 2)
                    Dim l As Decimal = list1((list1.Count / 2) - 1)
                    median.Text = (f + l) / 2
                End If
                mode.Text = "--"
                ' variance
                Dim totalDifference As Decimal = 0
                For i = 0 To list1.Count - 1
                    totalDifference += Math.Pow((list1(i) - (total / (list1.Count))), 2)
                Next
                variance.Text = Math.Round(totalDifference / (list1.Count - 1), 8)
                ' standard deviation
                st_deviation.Text = Math.Round(Math.Sqrt(totalDifference / (list1.Count - 1)), 8)
            ElseIf RadioButton2.Checked Then
                'grouped
                Dim list2 As ArrayList = New ArrayList()
                Dim list3 As ArrayList = New ArrayList()

                While sr.Peek <> -1
                    Dim DataRow As String() = sr.ReadLine().Split(",")
                    list2.Add(CDec(DataRow(0)))
                    list3.Add(CDec(DataRow(1)))
                End While
                maximum.Text = list2(list2.Count - 1)
                minimum.Text = list2(0)
                midrange.Text = (list2(list2.Count - 1) + list2(0)) / 2
                Dim x As Integer = 0
                Dim sumgroup As Decimal = 0
                For x = 0 To list2.Count - 1
                    sumgroup += list2(x) * list3(x)
                Next
                sum.Text = sumgroup
                Dim i1 As Integer = 0
                Dim total1 As Decimal = 0
                For i1 = 0 To list3.Count - 1
                    total1 += list3(i1)
                Next
                count.Text = total1
                mean.Text = Math.Round((sumgroup / total1), 8)
                Dim n As Integer = (total1 + 1) / 2
                Dim i2 As Integer = 0
                Dim cf As Integer = 0
                Dim cfreq As ArrayList = New ArrayList()
                For Each it2 In groupListY
                    If it2.Text <> "" Then
                        cf += it2.Text
                        cfreq.Add(CDec(cf))
                    End If
                Next
                Dim y As Integer = 0
                For y = 0 To cfreq.Count - 1
                    If n <= cfreq(y) Then
                        median.Text = list2(y)
                        Exit For
                    End If
                Next
                Dim z As Integer = 0
                Dim large As Integer = list3(0)
                For z = 1 To list3.Count - 1
                    If large < list3(z) Then
                        large = list3(z)
                        mode.Text = list2(z)
                    End If
                Next
                Dim x1 As Integer = 0
                Dim variancesum As Decimal = 0
                For x1 = 0 To list2.Count - 1
                    variancesum += (Math.Pow(list2(x1), 2)) * list3(x1)
                Next
                variance.Text = Math.Round(((variancesum - ((sumgroup * sumgroup) / total1)) / total1), 8)
                st_deviation.Text = Math.Round(Math.Sqrt(((variancesum - ((sumgroup * sumgroup) / total1)) / total1)), 8)
            Else
                'frequency
                Dim startinterval As ArrayList = New ArrayList()
                Dim endinterval As ArrayList = New ArrayList()
                Dim frequency As ArrayList = New ArrayList()
                While sr.Peek <> -1
                    Dim DataRow As String() = sr.ReadLine().Split(",")
                    startinterval.Add(CDec(DataRow(0)))
                    endinterval.Add(CDec(DataRow(1)))
                    frequency.Add(CDec(DataRow(2)))
                End While
                maximum.Text = endinterval(endinterval.Count - 1)
                minimum.Text = startinterval(0)
                midrange.Text = (endinterval(endinterval.Count - 1) + startinterval(0)) / 2
                Dim mid As ArrayList = New ArrayList()
                Dim i As Integer = 0
                For i = 0 To startinterval.Count - 1
                    mid.Add(CDec((startinterval(i) + endinterval(i)) / 2))
                Next
                Dim x As Integer = 0
                Dim sumgroup As Decimal = 0
                For x = 0 To mid.Count - 1
                    sumgroup += mid(x) * frequency(x)
                Next
                sum.Text = sumgroup
                Dim i1 As Integer = 0
                Dim total1 As Decimal = 0
                For i1 = 0 To frequency.Count - 1
                    total1 += frequency(i1)
                Next
                count.Text = total1
                mean.Text = Math.Round((sumgroup / total1), 8)
                Dim n As Decimal = total1 / 2
                Dim i2 As Integer = 0
                Dim cf As Integer = 0
                Dim cfreq As ArrayList = New ArrayList()
                For Each it2 In freqList
                    If it2.Text <> "" Then
                        cf += it2.Text
                        cfreq.Add(CDec(cf))
                    End If
                Next
                Dim y As Integer = 0
                For y = 0 To cfreq.Count - 1
                    If n <= cfreq(y) Then
                        median.Text = Math.Round((startinterval(y) + ((n - (cfreq(y - 1))) * (endinterval(0) - startinterval(0))) / frequency(y)), 8)
                        Exit For
                    End If
                Next
                Dim z As Integer = 0
                Dim large As Integer = frequency(0)
                For z = 1 To frequency.Count - 1
                    If large < frequency(z) Then
                        large = frequency(z)
                        mode.Text = mid(z)
                    End If
                Next
                Dim x1 As Integer = 0
                Dim variancesum As Decimal = 0
                For x1 = 0 To mid.Count - 1
                    variancesum += (Math.Pow(mid(x1), 2)) * frequency(x1)
                Next
                variance.Text = Math.Round(((variancesum - ((sumgroup * sumgroup) / total1)) / total1), 8)
                st_deviation.Text = Math.Round(Math.Sqrt(((variancesum - ((sumgroup * sumgroup) / total1)) / total1)), 8)
            End If
        End If
    End Sub
End Class