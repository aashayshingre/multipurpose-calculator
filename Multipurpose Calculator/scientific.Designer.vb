﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class scientific
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.expression_box = New System.Windows.Forms.TextBox()
        Me.answerbox = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.load_c = New System.Windows.Forms.Button()
        Me.load_b = New System.Windows.Forms.Button()
        Me.load_a = New System.Windows.Forms.Button()
        Me.store_c = New System.Windows.Forms.Button()
        Me.store_b = New System.Windows.Forms.Button()
        Me.store_a = New System.Windows.Forms.Button()
        Me.equalbutton = New System.Windows.Forms.Button()
        Me.decimalpoint = New System.Windows.Forms.Button()
        Me.key0 = New System.Windows.Forms.Button()
        Me.powbutton = New System.Windows.Forms.Button()
        Me.rightpar = New System.Windows.Forms.Button()
        Me.leftpar = New System.Windows.Forms.Button()
        Me.key3 = New System.Windows.Forms.Button()
        Me.key2 = New System.Windows.Forms.Button()
        Me.key1 = New System.Windows.Forms.Button()
        Me.subbutton = New System.Windows.Forms.Button()
        Me.addbutton = New System.Windows.Forms.Button()
        Me.key6 = New System.Windows.Forms.Button()
        Me.key5 = New System.Windows.Forms.Button()
        Me.key4 = New System.Windows.Forms.Button()
        Me.delbutton = New System.Windows.Forms.Button()
        Me.multbutton = New System.Windows.Forms.Button()
        Me.divbutton = New System.Windows.Forms.Button()
        Me.key9 = New System.Windows.Forms.Button()
        Me.key8 = New System.Windows.Forms.Button()
        Me.key7 = New System.Windows.Forms.Button()
        Me.clearbutton = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.cube = New System.Windows.Forms.Button()
        Me.inverse = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.squareroot = New System.Windows.Forms.Button()
        Me.cuberoot = New System.Windows.Forms.Button()
        Me.square = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.answer = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.expression_box, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.answerbox, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1028, 495)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'expression_box
        '
        Me.expression_box.AccessibleRole = System.Windows.Forms.AccessibleRole.Equation
        Me.expression_box.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.SetColumnSpan(Me.expression_box, 3)
        Me.expression_box.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.expression_box.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.expression_box.Location = New System.Drawing.Point(53, 26)
        Me.expression_box.Margin = New System.Windows.Forms.Padding(2)
        Me.expression_box.Name = "expression_box"
        Me.expression_box.ReadOnly = True
        Me.expression_box.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.expression_box.Size = New System.Drawing.Size(919, 33)
        Me.expression_box.TabIndex = 0
        Me.expression_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'answerbox
        '
        Me.answerbox.Dock = System.Windows.Forms.DockStyle.Top
        Me.answerbox.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.answerbox.Location = New System.Drawing.Point(540, 73)
        Me.answerbox.Margin = New System.Windows.Forms.Padding(2, 12, 2, 2)
        Me.answerbox.Name = "answerbox"
        Me.answerbox.Size = New System.Drawing.Size(432, 33)
        Me.answerbox.TabIndex = 1
        Me.answerbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TableLayoutPanel2.ColumnCount = 6
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.Controls.Add(Me.load_c, 5, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.load_b, 4, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.load_a, 3, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.store_c, 2, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.store_b, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.store_a, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.equalbutton, 3, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.decimalpoint, 2, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.key0, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.powbutton, 5, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.rightpar, 4, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.leftpar, 3, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.key3, 2, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.key2, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.key1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.subbutton, 4, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.addbutton, 3, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.key6, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.key5, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.key4, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.delbutton, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.multbutton, 4, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.divbutton, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.key9, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.key8, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.key7, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.clearbutton, 5, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(53, 124)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(432, 317)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'load_c
        '
        Me.load_c.Dock = System.Windows.Forms.DockStyle.Fill
        Me.load_c.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.load_c.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.load_c.Location = New System.Drawing.Point(357, 254)
        Me.load_c.Margin = New System.Windows.Forms.Padding(2)
        Me.load_c.Name = "load_c"
        Me.load_c.Size = New System.Drawing.Size(73, 61)
        Me.load_c.TabIndex = 26
        Me.load_c.Text = "Z"
        Me.load_c.UseVisualStyleBackColor = True
        '
        'load_b
        '
        Me.load_b.Dock = System.Windows.Forms.DockStyle.Fill
        Me.load_b.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.load_b.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.load_b.Location = New System.Drawing.Point(286, 254)
        Me.load_b.Margin = New System.Windows.Forms.Padding(2)
        Me.load_b.Name = "load_b"
        Me.load_b.Size = New System.Drawing.Size(67, 61)
        Me.load_b.TabIndex = 25
        Me.load_b.Text = "Y"
        Me.load_b.UseVisualStyleBackColor = True
        '
        'load_a
        '
        Me.load_a.Dock = System.Windows.Forms.DockStyle.Fill
        Me.load_a.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.load_a.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.load_a.Location = New System.Drawing.Point(215, 254)
        Me.load_a.Margin = New System.Windows.Forms.Padding(2)
        Me.load_a.Name = "load_a"
        Me.load_a.Size = New System.Drawing.Size(67, 61)
        Me.load_a.TabIndex = 24
        Me.load_a.Text = "X"
        Me.load_a.UseVisualStyleBackColor = True
        '
        'store_c
        '
        Me.store_c.Dock = System.Windows.Forms.DockStyle.Fill
        Me.store_c.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.store_c.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.store_c.Location = New System.Drawing.Point(144, 254)
        Me.store_c.Margin = New System.Windows.Forms.Padding(2)
        Me.store_c.Name = "store_c"
        Me.store_c.Size = New System.Drawing.Size(67, 61)
        Me.store_c.TabIndex = 23
        Me.store_c.Text = "STORE IN  Z"
        Me.store_c.UseVisualStyleBackColor = True
        '
        'store_b
        '
        Me.store_b.Dock = System.Windows.Forms.DockStyle.Fill
        Me.store_b.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.store_b.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.store_b.Location = New System.Drawing.Point(73, 254)
        Me.store_b.Margin = New System.Windows.Forms.Padding(2)
        Me.store_b.Name = "store_b"
        Me.store_b.Size = New System.Drawing.Size(67, 61)
        Me.store_b.TabIndex = 22
        Me.store_b.Text = "STORE IN Y"
        Me.store_b.UseVisualStyleBackColor = True
        '
        'store_a
        '
        Me.store_a.Dock = System.Windows.Forms.DockStyle.Fill
        Me.store_a.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.store_a.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.store_a.Location = New System.Drawing.Point(2, 254)
        Me.store_a.Margin = New System.Windows.Forms.Padding(2)
        Me.store_a.Name = "store_a"
        Me.store_a.Size = New System.Drawing.Size(67, 61)
        Me.store_a.TabIndex = 21
        Me.store_a.Text = "STORE IN X"
        Me.store_a.UseVisualStyleBackColor = True
        '
        'equalbutton
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.equalbutton, 3)
        Me.equalbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.equalbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.equalbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.equalbutton.Location = New System.Drawing.Point(215, 191)
        Me.equalbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.equalbutton.Name = "equalbutton"
        Me.equalbutton.Size = New System.Drawing.Size(215, 59)
        Me.equalbutton.TabIndex = 20
        Me.equalbutton.Text = "="
        Me.equalbutton.UseVisualStyleBackColor = True
        '
        'decimalpoint
        '
        Me.decimalpoint.Dock = System.Windows.Forms.DockStyle.Fill
        Me.decimalpoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.decimalpoint.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.decimalpoint.Location = New System.Drawing.Point(144, 191)
        Me.decimalpoint.Margin = New System.Windows.Forms.Padding(2)
        Me.decimalpoint.Name = "decimalpoint"
        Me.decimalpoint.Size = New System.Drawing.Size(67, 59)
        Me.decimalpoint.TabIndex = 19
        Me.decimalpoint.Text = "."
        Me.decimalpoint.UseVisualStyleBackColor = True
        '
        'key0
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.key0, 2)
        Me.key0.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key0.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key0.Location = New System.Drawing.Point(2, 191)
        Me.key0.Margin = New System.Windows.Forms.Padding(2)
        Me.key0.Name = "key0"
        Me.key0.Size = New System.Drawing.Size(138, 59)
        Me.key0.TabIndex = 18
        Me.key0.Text = "0"
        Me.key0.UseVisualStyleBackColor = True
        '
        'powbutton
        '
        Me.powbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.powbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.powbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.powbutton.Location = New System.Drawing.Point(357, 128)
        Me.powbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.powbutton.Name = "powbutton"
        Me.powbutton.Size = New System.Drawing.Size(73, 59)
        Me.powbutton.TabIndex = 17
        Me.powbutton.Text = "^"
        Me.powbutton.UseVisualStyleBackColor = True
        '
        'rightpar
        '
        Me.rightpar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rightpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rightpar.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rightpar.Location = New System.Drawing.Point(286, 128)
        Me.rightpar.Margin = New System.Windows.Forms.Padding(2)
        Me.rightpar.Name = "rightpar"
        Me.rightpar.Size = New System.Drawing.Size(67, 59)
        Me.rightpar.TabIndex = 16
        Me.rightpar.Text = ")"
        Me.rightpar.UseVisualStyleBackColor = True
        '
        'leftpar
        '
        Me.leftpar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.leftpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.leftpar.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.leftpar.Location = New System.Drawing.Point(215, 128)
        Me.leftpar.Margin = New System.Windows.Forms.Padding(2)
        Me.leftpar.Name = "leftpar"
        Me.leftpar.Size = New System.Drawing.Size(67, 59)
        Me.leftpar.TabIndex = 15
        Me.leftpar.Text = "("
        Me.leftpar.UseVisualStyleBackColor = True
        '
        'key3
        '
        Me.key3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key3.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key3.Location = New System.Drawing.Point(144, 128)
        Me.key3.Margin = New System.Windows.Forms.Padding(2)
        Me.key3.Name = "key3"
        Me.key3.Size = New System.Drawing.Size(67, 59)
        Me.key3.TabIndex = 14
        Me.key3.Text = "3"
        Me.key3.UseVisualStyleBackColor = True
        '
        'key2
        '
        Me.key2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key2.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key2.Location = New System.Drawing.Point(73, 128)
        Me.key2.Margin = New System.Windows.Forms.Padding(2)
        Me.key2.Name = "key2"
        Me.key2.Size = New System.Drawing.Size(67, 59)
        Me.key2.TabIndex = 13
        Me.key2.Text = "2"
        Me.key2.UseVisualStyleBackColor = True
        '
        'key1
        '
        Me.key1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key1.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key1.Location = New System.Drawing.Point(2, 128)
        Me.key1.Margin = New System.Windows.Forms.Padding(2)
        Me.key1.Name = "key1"
        Me.key1.Size = New System.Drawing.Size(67, 59)
        Me.key1.TabIndex = 12
        Me.key1.Text = "1"
        Me.key1.UseVisualStyleBackColor = True
        '
        'subbutton
        '
        Me.subbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.subbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.subbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.subbutton.Location = New System.Drawing.Point(286, 65)
        Me.subbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.subbutton.Name = "subbutton"
        Me.subbutton.Size = New System.Drawing.Size(67, 59)
        Me.subbutton.TabIndex = 10
        Me.subbutton.Text = "−"
        Me.subbutton.UseVisualStyleBackColor = True
        '
        'addbutton
        '
        Me.addbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.addbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.addbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.addbutton.Location = New System.Drawing.Point(215, 65)
        Me.addbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.addbutton.Name = "addbutton"
        Me.addbutton.Size = New System.Drawing.Size(67, 59)
        Me.addbutton.TabIndex = 9
        Me.addbutton.Text = "+"
        Me.addbutton.UseVisualStyleBackColor = True
        '
        'key6
        '
        Me.key6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key6.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key6.Location = New System.Drawing.Point(144, 65)
        Me.key6.Margin = New System.Windows.Forms.Padding(2)
        Me.key6.Name = "key6"
        Me.key6.Size = New System.Drawing.Size(67, 59)
        Me.key6.TabIndex = 8
        Me.key6.Text = "6"
        Me.key6.UseVisualStyleBackColor = True
        '
        'key5
        '
        Me.key5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key5.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key5.Location = New System.Drawing.Point(73, 65)
        Me.key5.Margin = New System.Windows.Forms.Padding(2)
        Me.key5.Name = "key5"
        Me.key5.Size = New System.Drawing.Size(67, 59)
        Me.key5.TabIndex = 7
        Me.key5.Text = "5"
        Me.key5.UseVisualStyleBackColor = True
        '
        'key4
        '
        Me.key4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key4.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key4.Location = New System.Drawing.Point(2, 65)
        Me.key4.Margin = New System.Windows.Forms.Padding(2)
        Me.key4.Name = "key4"
        Me.key4.Size = New System.Drawing.Size(67, 59)
        Me.key4.TabIndex = 6
        Me.key4.Text = "4"
        Me.key4.UseVisualStyleBackColor = True
        '
        'delbutton
        '
        Me.delbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.delbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.delbutton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.delbutton.Location = New System.Drawing.Point(357, 2)
        Me.delbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.delbutton.Name = "delbutton"
        Me.delbutton.Size = New System.Drawing.Size(73, 59)
        Me.delbutton.TabIndex = 5
        Me.delbutton.Text = "DEL"
        Me.delbutton.UseVisualStyleBackColor = True
        '
        'multbutton
        '
        Me.multbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.multbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.multbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.multbutton.Location = New System.Drawing.Point(286, 2)
        Me.multbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.multbutton.Name = "multbutton"
        Me.multbutton.Size = New System.Drawing.Size(67, 59)
        Me.multbutton.TabIndex = 4
        Me.multbutton.Text = "×"
        Me.multbutton.UseVisualStyleBackColor = True
        '
        'divbutton
        '
        Me.divbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.divbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.divbutton.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.divbutton.Location = New System.Drawing.Point(215, 2)
        Me.divbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.divbutton.Name = "divbutton"
        Me.divbutton.Size = New System.Drawing.Size(67, 59)
        Me.divbutton.TabIndex = 3
        Me.divbutton.Text = "÷"
        Me.divbutton.UseVisualStyleBackColor = True
        '
        'key9
        '
        Me.key9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key9.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key9.Location = New System.Drawing.Point(144, 2)
        Me.key9.Margin = New System.Windows.Forms.Padding(2)
        Me.key9.Name = "key9"
        Me.key9.Size = New System.Drawing.Size(67, 59)
        Me.key9.TabIndex = 2
        Me.key9.Text = "9"
        Me.key9.UseVisualStyleBackColor = True
        '
        'key8
        '
        Me.key8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key8.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key8.Location = New System.Drawing.Point(73, 2)
        Me.key8.Margin = New System.Windows.Forms.Padding(2)
        Me.key8.Name = "key8"
        Me.key8.Size = New System.Drawing.Size(67, 59)
        Me.key8.TabIndex = 1
        Me.key8.Text = "8"
        Me.key8.UseVisualStyleBackColor = True
        '
        'key7
        '
        Me.key7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.key7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.key7.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.key7.Location = New System.Drawing.Point(2, 2)
        Me.key7.Margin = New System.Windows.Forms.Padding(2)
        Me.key7.Name = "key7"
        Me.key7.Size = New System.Drawing.Size(67, 59)
        Me.key7.TabIndex = 0
        Me.key7.Text = "7"
        Me.key7.UseVisualStyleBackColor = True
        '
        'clearbutton
        '
        Me.clearbutton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clearbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clearbutton.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clearbutton.Location = New System.Drawing.Point(357, 65)
        Me.clearbutton.Margin = New System.Windows.Forms.Padding(2)
        Me.clearbutton.Name = "clearbutton"
        Me.clearbutton.Size = New System.Drawing.Size(73, 59)
        Me.clearbutton.TabIndex = 27
        Me.clearbutton.Text = "CLEAR"
        Me.clearbutton.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 6
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button1, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button2, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button3, 2, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.cube, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.inverse, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button26, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.squareroot, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cuberoot, 4, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.square, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button21, 2, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.answer, 5, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button5, 3, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button4, 5, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button17, 4, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button19, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Button25, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Button7, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Button8, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Button20, 1, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Button9, 2, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Button27, 2, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Button30, 3, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Button10, 4, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Button11, 4, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Button12, 5, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Button29, 5, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Button28, 3, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Button6, 3, 4)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(541, 125)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 5
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(430, 315)
        Me.TableLayoutPanel3.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(3, 66)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 57)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "sin"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(74, 66)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 57)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "cos"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(145, 66)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(65, 57)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "tan"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'cube
        '
        Me.cube.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cube.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cube.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cube.Location = New System.Drawing.Point(74, 3)
        Me.cube.Name = "cube"
        Me.cube.Size = New System.Drawing.Size(65, 57)
        Me.cube.TabIndex = 13
        Me.cube.Text = "x³"
        Me.cube.UseVisualStyleBackColor = True
        '
        'inverse
        '
        Me.inverse.Dock = System.Windows.Forms.DockStyle.Fill
        Me.inverse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.inverse.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inverse.Location = New System.Drawing.Point(145, 3)
        Me.inverse.Name = "inverse"
        Me.inverse.Size = New System.Drawing.Size(65, 57)
        Me.inverse.TabIndex = 14
        Me.inverse.Text = "1/x"
        Me.inverse.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button26.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.Location = New System.Drawing.Point(74, 129)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(65, 57)
        Me.Button26.TabIndex = 25
        Me.Button26.Text = "cosh"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'squareroot
        '
        Me.squareroot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.squareroot.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.squareroot.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.squareroot.Location = New System.Drawing.Point(216, 3)
        Me.squareroot.Name = "squareroot"
        Me.squareroot.Size = New System.Drawing.Size(65, 57)
        Me.squareroot.TabIndex = 15
        Me.squareroot.Text = "√"
        Me.squareroot.UseVisualStyleBackColor = True
        '
        'cuberoot
        '
        Me.cuberoot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cuberoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cuberoot.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cuberoot.Location = New System.Drawing.Point(287, 3)
        Me.cuberoot.Name = "cuberoot"
        Me.cuberoot.Size = New System.Drawing.Size(65, 57)
        Me.cuberoot.TabIndex = 17
        Me.cuberoot.Text = "³√"
        Me.cuberoot.UseVisualStyleBackColor = True
        '
        'square
        '
        Me.square.Dock = System.Windows.Forms.DockStyle.Fill
        Me.square.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.square.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.square.Location = New System.Drawing.Point(3, 3)
        Me.square.Name = "square"
        Me.square.Size = New System.Drawing.Size(65, 57)
        Me.square.TabIndex = 12
        Me.square.Text = "x²"
        Me.square.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.Location = New System.Drawing.Point(145, 192)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(65, 57)
        Me.Button21.TabIndex = 20
        Me.Button21.Text = " tan ̄ ¹"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'answer
        '
        Me.answer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.answer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.answer.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.answer.Location = New System.Drawing.Point(358, 3)
        Me.answer.Name = "answer"
        Me.answer.Size = New System.Drawing.Size(69, 57)
        Me.answer.TabIndex = 5
        Me.answer.Text = "%"
        Me.answer.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(216, 66)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(65, 57)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "log"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(358, 66)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(69, 57)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "e"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button17.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button17.Location = New System.Drawing.Point(287, 66)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(65, 57)
        Me.Button17.TabIndex = 16
        Me.Button17.Text = "logₑ"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.Location = New System.Drawing.Point(3, 255)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(65, 57)
        Me.Button19.TabIndex = 18
        Me.Button19.Text = "sinh ̄ ¹"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button25.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.Location = New System.Drawing.Point(3, 129)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(65, 57)
        Me.Button25.TabIndex = 24
        Me.Button25.Text = "sinh"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(3, 192)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(65, 57)
        Me.Button7.TabIndex = 6
        Me.Button7.Text = " sin ̄ ¹"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(74, 192)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(65, 57)
        Me.Button8.TabIndex = 7
        Me.Button8.Text = " cos ̄ ¹"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.Location = New System.Drawing.Point(74, 255)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(65, 57)
        Me.Button20.TabIndex = 19
        Me.Button20.Text = "cosh ̄ ¹"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(145, 255)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(65, 57)
        Me.Button9.TabIndex = 8
        Me.Button9.Text = " tanh ̄ ¹"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button27.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button27.Location = New System.Drawing.Point(145, 129)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(65, 57)
        Me.Button27.TabIndex = 26
        Me.Button27.Text = "tanh"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button30
        '
        Me.Button30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button30.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.Location = New System.Drawing.Point(216, 129)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(65, 57)
        Me.Button30.TabIndex = 29
        Me.Button30.Text = "x10˟"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(287, 192)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(65, 57)
        Me.Button10.TabIndex = 9
        Me.Button10.Text = "nPr"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.Location = New System.Drawing.Point(287, 129)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(65, 57)
        Me.Button11.TabIndex = 10
        Me.Button11.Text = "exp"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(358, 192)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(69, 57)
        Me.Button12.TabIndex = 11
        Me.Button12.Text = "!"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.BackColor = System.Drawing.SystemColors.Control
        Me.Button29.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button29.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button29.Location = New System.Drawing.Point(358, 129)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(69, 57)
        Me.Button29.TabIndex = 28
        Me.Button29.Text = "π"
        Me.Button29.UseVisualStyleBackColor = False
        '
        'Button28
        '
        Me.Button28.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button28.Font = New System.Drawing.Font("Trebuchet MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.Location = New System.Drawing.Point(216, 192)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(65, 57)
        Me.Button28.TabIndex = 27
        Me.Button28.Text = "nCr"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.TableLayoutPanel3.SetColumnSpan(Me.Button6, 3)
        Me.Button6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(216, 255)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(211, 57)
        Me.Button6.TabIndex = 30
        Me.Button6.Text = "Ans"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Lucida Sans Typewriter", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GroupBox1.Location = New System.Drawing.Point(56, 61)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(5, 0, 0, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(8, 3, 3, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(225, 37)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Angle"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Font = New System.Drawing.Font("Bahnschrift SemiLight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(96, 20)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(61, 18)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Radian"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Bahnschrift SemiLight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(11, 20)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(62, 18)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.Text = "Degree"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'scientific
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 495)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(1027, 534)
        Me.Name = "scientific"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Scientific Calculator"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents expression_box As TextBox
    Friend WithEvents answerbox As TextBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents key9 As Button
    Friend WithEvents key8 As Button
    Friend WithEvents key7 As Button
    Friend WithEvents load_c As Button
    Friend WithEvents load_b As Button
    Friend WithEvents load_a As Button
    Friend WithEvents store_c As Button
    Friend WithEvents store_b As Button
    Friend WithEvents store_a As Button
    Friend WithEvents equalbutton As Button
    Friend WithEvents decimalpoint As Button
    Friend WithEvents key0 As Button
    Friend WithEvents powbutton As Button
    Friend WithEvents rightpar As Button
    Friend WithEvents leftpar As Button
    Friend WithEvents key3 As Button
    Friend WithEvents key2 As Button
    Friend WithEvents key1 As Button
    Friend WithEvents subbutton As Button
    Friend WithEvents addbutton As Button
    Friend WithEvents key6 As Button
    Friend WithEvents key5 As Button
    Friend WithEvents key4 As Button
    Friend WithEvents multbutton As Button
    Friend WithEvents divbutton As Button
    Friend WithEvents delbutton As Button
    Friend WithEvents clearbutton As Button
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents cube As Button
    Friend WithEvents inverse As Button
    Friend WithEvents squareroot As Button
    Friend WithEvents cuberoot As Button
    Friend WithEvents square As Button
    Friend WithEvents Button19 As Button
    Friend WithEvents Button20 As Button
    Friend WithEvents Button21 As Button
    Friend WithEvents answer As Button
    Friend WithEvents Button17 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents Button30 As Button
    Friend WithEvents Button25 As Button
    Friend WithEvents Button26 As Button
    Friend WithEvents Button27 As Button
    Friend WithEvents Button28 As Button
    Friend WithEvents Button29 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Button6 As Button
End Class
